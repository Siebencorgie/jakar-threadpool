extern crate num_cpus;

use std::thread::*;
use std::sync::mpsc::*;
use std::sync::{Arc, Mutex};
use std::time::Duration;

use std::result::Result;
use std::sync::TryLockError;
use std::collections::VecDeque;

pub mod job;


struct Worker {
    name: String,
    thread_handle: Option<JoinHandle<()>>,
    jobs: Arc<Mutex<VecDeque<job::ThreadJob>>>
}

impl Worker{
    ///Starts this worker
    pub fn start(
        name: String,
        jobs: Arc<Mutex<VecDeque<job::ThreadJob>>>,
    ) -> Self{

        //TODO use builder for thread
        let thread_name = name.clone();
        let builder = Builder::new().name(name.clone());

        let inner_jobs = jobs.clone();

        let thread_handle: JoinHandle<()> = builder.spawn(move || {
            let thread_jobs = inner_jobs;

            loop {

                let job = {
                    match thread_jobs.lock(){
                        Ok(mut job_vec) => match job_vec.pop_back(){
                            Some(job) => job,
                            None => {
                                job::ThreadJob::Wait(Duration::from_millis(10))
                            }
                        },
                        Err(_) => {
                            println!("Job Vec for thread {} has died, returning.", thread_name);
                            return;
                        }
                    }
                };

                //If we got this job, let's execute it
                match job{
                    job::ThreadJob::Do(func) => {
                        func.exec_box();
                    },
                    job::ThreadJob::End => {
                        break;
                    } //end the thread
                    job::ThreadJob::Wait(dur) => sleep(dur),
                }
            }
        }).expect("Failed to spawn thread!");

        Worker{
            name: name,
            thread_handle: Some(thread_handle),
            jobs: jobs,
        }
    }
}

impl Drop for Worker{
    fn drop(&mut self) {
        println!("Ending worker {}", self.name);
        if let Some(handel) = self.thread_handle.take(){
            match handel.join(){
                Ok(_) => {},
                Err(_) => println!("Failed to join thread {}", self.name),
            }
        }else{
            return; //don't need to join
        }
    }
}


///Creates a pool of threads which can execute different tasks based on what is supplied
pub struct ThreadPool {
    name: String,
    pool: Vec<Worker>,
    jobs: Arc<Mutex<VecDeque<job::ThreadJob>>>,
}

impl ThreadPool{
    ///Creates a threadpool with the current hardware core count as `size`.
    pub fn new_hardware_optimal(name: String) -> Self{
        let size = num_cpus::get_physical() as u32;
        ThreadPool::new(size, name)
    }

    ///Creats a threadpool with the systems logical core count as `size`.
    pub fn new_logical_optimal(name: String) -> Self{
        let size = num_cpus::get() as u32;
        ThreadPool::new(size, name)
    }

    ///Creates the threadpool with the given number of threads
    pub fn new(size: u32, name: String) -> Self{
        let mut threads = Vec::new();
        //init communication channel
        let jobs_vec = Arc::new(Mutex::new(VecDeque::new()));

        for idx in 0..size{
            threads.push(Worker::start(
                name.clone() + "_" + &idx.to_string(),
                jobs_vec.clone()
            ));
        }

        ThreadPool{
            name: name,
            pool: threads,
            jobs: jobs_vec,
        }
    }

    ///Takes a closure which will be executed on one of the threads.
    pub fn execute<T>(&mut self, job: T) where T: FnOnce() + Send + 'static{
        //Create the job box, then send
        let job_box = job::ThreadJob::Do(Box::new(job));

        self.jobs.lock().expect("Failed to add new job to thread pool")
        .push_front(job_box);
        /*
        match self.sender.send(job_box){
            Ok(_) => {},
            Err(error) => {
                println!("failed to send job: {}", error);
            }
        }
        */
    }

    ///Adds one more worker to this ThreadPool
    pub fn add_worker(&mut self){
        let name = self.name.clone() + "_" + &self.pool.len().to_string();
        self.pool.push(Worker::start(name, self.jobs.clone()));
    }

    ///Returns the size of this threadpool.
    pub fn len(&self) -> usize{
        self.pool.len()
    }

    ///Blocks until all threads have finished
    pub fn wait(&mut self){
        //Wait a bit for the event that we call the wait directly after startin
        sleep(Duration::from_millis(10));
        //Check all workers and check if we have any, maybe unfinished jobs left
        while self.jobs.lock().expect("Failed to check job length in thread pool").len() > 0 {
            sleep(Duration::from_millis(10));
        }
        println!("Finished waiting", );
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        //First end all threads, then go out of scope
        for _ in self.pool.iter(){
            self.jobs.lock().expect("Failed to send stop job t threads")
            .push_front(job::ThreadJob::End);
        }
    }
}
